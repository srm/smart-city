# Smart Parking

* ## Dépendances 
    * Pour compiler vous avez besoin d'avoir la librairie [SFML](https://www.sfml-dev.org/index-fr.php) installée.


* ## Comment générer le Makefile avec Cmake : 

    * `cd [CHEMIN-VERS-REPO]/smart-city`
    * `cmake -B build`

* ## Comment compiler avec le Makefile:
    * `make -C build`

* ## Comment executer :
    * `./bin/smart-city_exec`
